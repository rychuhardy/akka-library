package com.company.client;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ry on 16.05.2017.
 */
public class Client {

    public static void printHelp() {
        System.out.println("---------------USAGE---------------");
        System.out.println("find `title`\tFinds price of given book");
        System.out.println("order `title`\tPlaces order for the given book");
        System.out.println("stream `title`\tStreams text from the book");

    }

    public static void main(String[] args) {
        // config
        File configFile = new File("client.conf");
        Config config = ConfigFactory.parseFile(configFile);

        // create actor system & actors
        final ActorSystem system = ActorSystem.create("client_system", config);
        final ActorRef actorRef = system.actorOf(Props.create(ClientActor.class), "client");

        // interaction
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String line = null;
            try {
                line = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (line.equals("q")) {
                break;
            }
            else if (line.equals("help")) {
                printHelp();
            }
            else {
                actorRef.tell(line, null);
            }
        }

        system.terminate();
    }
}
