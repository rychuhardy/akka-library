package com.company.client;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.company.messages.*;

/**
 * Created by ry on 16.05.2017.
 */
public class ClientActor extends AbstractActor {

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(String.class, s -> {
                    Object request = null;
                    log.info(s);
                    if(s.startsWith("find ")) {
                        String title = s.substring(5);
                        request = new SearchBookRequest(title);
                    }
                    else if(s.startsWith("order ")) {
                        String title = s.substring(6);
                        request = new OrderBookRequest(title);
                    }
                    else if(s.startsWith("stream ")) {
                        String title = s.substring(7);
                        request = new StreamRequest(title, getSelf());
                    }
                    else {
                        log.warning("Invalid request");
                    }

                    if(request != null) {
                        getContext().actorSelection("akka.tcp://library@127.0.0.1:3552/user/serv")
                                .tell(request, getSelf());
                    }
                })
                .match(SearchResult.class, s -> {
                    StringBuilder message = new StringBuilder();
                    message.append("Received results of search: \n");
                    if (s.isFound()) {
                        message.append("Price of the book: ");
                        message.append(s.getBookPrice());
                    }
                    else {
                        message.append("No results");
                    }
                    log.info(message.toString());
                })
                .match(OrderConfirmation.class, s -> {
                    StringBuilder message = new StringBuilder();
                    message.append("Received order confirmation: \n");
                    if (s.isOk()) {
                        message.append("Order completed successfully.\n");
                        if (s.getMessage().length() > 0) {
                            message.append("Additional message:\n");
                            message.append(s.getMessage());
                        }
                    }
                    else {
                        message.append("An error occurred: \n");
                        message.append(s.getMessage());
                    }
                    log.info(message.toString());
                })
                .match(StreamLine.class, s -> {
                    System.out.println("STREAM:\t" + s.getLine());
                })
                .matchAny(o -> log.info("ClientActor: Received unknown message"))
                .build();
    }
}
