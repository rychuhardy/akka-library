package com.company.messages;

import java.io.Serializable;

/**
 * Created by ry on 21-May-17.
 */
public class StreamLine implements Serializable{
    private final String line;

    public StreamLine(String line) {
        this.line = line;
    }

    public String getLine() {
        return line;
    }
}
