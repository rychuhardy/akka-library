package com.company.messages;

import java.io.Serializable;

/**
 * Created by ry on 16.05.2017.
 */
public class OrderConfirmation implements Serializable {
    private final boolean isOk;
    private final String message;

    public OrderConfirmation(boolean isOk, String message) {
        this.isOk = isOk;
        this.message = message;
    }

    public boolean isOk() {
        return isOk;
    }

    public String getMessage() {
        return message;
    }

}
