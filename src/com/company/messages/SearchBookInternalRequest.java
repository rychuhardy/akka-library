package com.company.messages;

import java.io.Serializable;

/**
 * Created by ry on 16.05.2017.
 */
public class SearchBookInternalRequest implements Serializable {
    private final SearchBookRequest request;
    private final String id;

    public SearchBookInternalRequest(SearchBookRequest request, String id) {
        this.request = request;
        this.id = id;
    }

    public SearchBookRequest getRequest() {
        return request;
    }

    public String getId() {
        return id;
    }
}
