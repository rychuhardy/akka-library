package com.company.messages;

import akka.actor.ActorRef;

import java.io.Serializable;

/**
 * Created by ry on 21-May-17.
 */
public class StreamRequest implements Serializable {
    private final String bookName;
    private final ActorRef requester;
    public StreamRequest(String bookName, ActorRef requester) {
        this.bookName = bookName;
        this.requester = requester;
    }

    public String getBookName() {
        return bookName;
    }

    public ActorRef getRequester() {
        return requester;
    }
}
