package com.company.messages;

import java.io.Serializable;

/**
 * Created by ry on 16.05.2017.
 */
public class OrderBookRequest implements Serializable {
    private final String title;

    public OrderBookRequest(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

}
