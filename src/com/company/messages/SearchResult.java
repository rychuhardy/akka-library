package com.company.messages;

import java.io.Serializable;

/**
 * Created by ry on 16.05.2017.
 */
public class SearchResult implements Serializable {
    private final String id;
    private final boolean isFound;
    private final int bookPrice;

    public SearchResult(String id) {
        this(id, false, -1);
    }

    public SearchResult(String id, int bookPrice) {
        this(id, true, bookPrice);
    }

    private SearchResult(String id, boolean isFound, int bookPrice) {
        this.id = id;
        this.isFound = isFound;
        this.bookPrice = bookPrice;
    }

    public boolean isFound() {
        return isFound;
    }

    public int getBookPrice() {
        return bookPrice;
    }

    public String getId() {
        return id;
    }
}
