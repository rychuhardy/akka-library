package com.company.server.actors.orders;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.company.messages.OrderBookRequest;
import com.company.messages.OrderConfirmation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


/**
 * Created by ry on 16.05.2017.
 */
public class OrderMaker extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);


    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(OrderBookRequest.class, s -> {
                    try {
                        appendToFile(s.getTitle() + System.lineSeparator());
                        getSender().tell(new OrderConfirmation(true, ""), getSelf());
                    } catch (IOException ex) {
                        log.error("OrderMaker: " + ex.getMessage());
                        getSender().tell(new OrderConfirmation(false, "IOException " + ex.getMessage()), getSelf());
                    }

                })
                .matchAny(o -> log.info("OrderMaker: received unknown message"))
                .build();
    }

    private synchronized void appendToFile(String line) throws IOException {
        Writer output = new BufferedWriter(new FileWriter("orders.txt", true));
        output.append(line);
        output.close();
    }
}
