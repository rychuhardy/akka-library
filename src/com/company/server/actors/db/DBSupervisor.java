package com.company.server.actors.db;

import akka.actor.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.DeciderBuilder;
import com.company.messages.SearchBookInternalRequest;
import com.company.messages.SearchBookRequest;
import static akka.actor.SupervisorStrategy.restart;
import com.company.messages.SearchResult;
import scala.concurrent.duration.Duration;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by ry on 16.05.2017.
 */
public class DBSupervisor extends AbstractActor{

    private class StateOfSearch {
        private ActorRef actorRef;
        private int responsesReceived;
        private SearchResult result;
        private boolean isResponseSent;

        public StateOfSearch(ActorRef actorRef) {
            this.actorRef = actorRef;
            this.responsesReceived = 0;
            this.isResponseSent = false;
        }

        public ActorRef getActorRef() {
            return actorRef;
        }

        public int getResponsesReceived() {
            return responsesReceived;
        }

        public SearchResult getResult() {
            return result;
        }

        public void addResponse() {
            this.responsesReceived++;
        }

        public void addResponse(SearchResult result) {
            addResponse();
            this.result = result;
        }

        public boolean isResponseSent() {
            return isResponseSent;
        }

        public void setResponseSent() {
            isResponseSent = true;
        }
    }

    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    private final int DBCount = 2;
    private final Map<String, StateOfSearch> requests;

    public DBSupervisor() {
        this.requests = new HashMap<>();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(SearchBookRequest.class, s -> {
                    String key = UUID.randomUUID().toString();
                    requests.put(key, new StateOfSearch(getSender()));
                    for(int i = 1; i <= DBCount; ++i) {
                        ActorRef actorRef = context().actorOf(Props.create(DBSearcher.class, "db_" + Integer.toString(i) + ".txt"));
                        actorRef.tell(new SearchBookInternalRequest(s, key), getSelf());
                    }
                })
                .match(SearchResult.class, s -> {
                    StateOfSearch state = requests.get(s.getId());
                    if (state == null) return;
                    if (s.isFound() && !state.isResponseSent()) {
                        state.addResponse(s);
                        state.getActorRef().tell(state.getResult(), getSelf());
                        state.setResponseSent();
                    }
                    else {
                        state.addResponse();
                    }
                    if(state.getResponsesReceived() == DBCount) {
                        if(!state.isResponseSent()) { // send information about not finding anything
                            state.getActorRef().tell(s, getSelf());
                            state.setResponseSent();
                        }
                        requests.remove(s.getId());
                    }
                })
                .matchAny(o -> log.info("DBSupervisor: received unknown message"))
                .build();
    }

    @Override
    public void preStart() throws Exception {
//        for(int i = 1; i <= DBCount; ++i) {
//            context().actorOf(Props.create(DBSearcher.class, "db_" + Integer.toString(i) + ".txt"), "dbsearcher_" + Integer.toString(i));
//        }
        log.info("DBSupervisor prestart complete");

    }

    private static SupervisorStrategy strategy
            = new OneForOneStrategy(10, Duration.create("1 minute"), DeciderBuilder
                    .matchAny(o -> restart())
                    .build());

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

}
