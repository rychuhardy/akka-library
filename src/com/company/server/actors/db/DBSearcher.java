package com.company.server.actors.db;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.company.messages.SearchBookInternalRequest;
import com.company.messages.SearchResult;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Created by ry on 16.05.2017.
 */
public class DBSearcher extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    private final String dbName;

    public DBSearcher(String dbName) {
        this.dbName = dbName;
    }


    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(SearchBookInternalRequest.class, s -> {
                    Path path = Paths.get(dbName);
                    SearchResult result = new SearchResult(s.getId());
                    try(Stream<String> lines = Files.lines(path)) {
                        String[] linesArray = lines.toArray(String[]::new);
                        for(String line: linesArray) {
                            if(line.startsWith("\"")) {
                                line = line.substring(1);
                            }

                            if (line.startsWith(s.getRequest().getTitle())) {
                                int price = Integer.parseInt(line.substring(line.lastIndexOf(" ") + 1));
                                result = new SearchResult(s.getId(), price);
                                break;
                            }
                        }
                    }
                    finally {
                        getSender().tell(result, getSelf());
                    }
                })
                .matchAny(o -> log.info("DBSearcher: received unknown message"))
                .build();
    }

}
