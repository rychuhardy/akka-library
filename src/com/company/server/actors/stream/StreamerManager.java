package com.company.server.actors.stream;

import akka.Done;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.function.Function;
import akka.stream.*;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Framing;
import akka.stream.javadsl.FramingTruncation;
import akka.stream.javadsl.Sink;
import akka.util.ByteString;
import com.company.messages.StreamLine;
import com.company.messages.StreamRequest;
import scala.concurrent.duration.Duration;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

/**
 * Created by ry on 21-May-17.
 */
public class StreamerManager extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(StreamRequest.class, s -> {
                    final Path path = Paths.get(s.getBookName());
                    final Function<Throwable, Supervision.Directive> decider = exc -> { return Supervision.resume();};
                    final Materializer mat = ActorMaterializer.create(
                            ActorMaterializerSettings.create(
                            getContext().getSystem()).withSupervisionStrategy(decider),
                            getContext().getSystem());
                    Sink<ByteString, CompletionStage<Done>> sendSink =
                            Sink.<ByteString> foreach(line -> {
                                s.getRequester().tell(new StreamLine(line.decodeString(ByteString.UTF_8())), getSelf());});

                    final CompletionStage<IOResult> ioResult =
                            FileIO.fromPath(path, 1)
                                    .via(Framing.delimiter(ByteString.fromString("\n"), Integer.MAX_VALUE, FramingTruncation.DISALLOW))
                                    .throttle(1, Duration.create(1, TimeUnit.SECONDS), 1, ThrottleMode.shaping())
                                    .to(sendSink)
                                    .run(mat);
                    ioResult.thenApply(d -> {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        s.getRequester().tell(new StreamLine("END OF STREAM"), getSelf());
                        return d;
                    });
                })
                .matchAny(o -> log.info("StreamerManager: received unknown message"))
                .build();
    }
}
