package com.company.server.actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.DeciderBuilder;
import akka.actor.OneForOneStrategy;
import com.company.messages.StreamRequest;
import com.company.server.actors.stream.StreamerManager;
import scala.concurrent.duration.Duration;
import akka.actor.SupervisorStrategy;
import static akka.actor.SupervisorStrategy.restart;
import com.company.messages.OrderBookRequest;
import com.company.messages.SearchBookRequest;
import com.company.server.actors.db.DBSupervisor;
import com.company.server.actors.orders.OrderMaker;

/**
 * Created by ry on 16.05.2017.
 */
public class Dispatcher extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(SearchBookRequest.class, s -> {
                    context().child("dbsupervisor").get().forward(s, getContext());
                })
                .match(OrderBookRequest.class, s -> {
                    context().child("ordermaker").get().forward(s, getContext());
                })
                .match(StreamRequest.class, s -> {
                    context().child("streamermanager").get().forward(s, getContext());
                })
                .matchAny(o -> log.info("Dispatcher: received unknown message"))
                .build();
    }

    @Override
    public void preStart() throws Exception {
        context().actorOf(Props.create(DBSupervisor.class), "dbsupervisor");
        context().actorOf(Props.create(OrderMaker.class), "ordermaker");
        context().actorOf(Props.create(StreamerManager.class), "streamermanager");
        log.info("Dispatcher prestart complete");
    }

    private static SupervisorStrategy strategy
            = new OneForOneStrategy(10, Duration.create("1 minute"), DeciderBuilder
                    .matchAny(o -> restart())
                    .build());

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }
}
