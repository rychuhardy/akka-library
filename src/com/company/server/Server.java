package com.company.server;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.company.server.actors.Dispatcher;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;

public class Server {

    public static void main(String[] args) {
            File configFile = new File("server.conf");
            Config config = ConfigFactory.parseFile(configFile);

            // create actor system & actors
            final ActorSystem system = ActorSystem.create("library", config);
            final ActorRef dispatcher = system.actorOf(Props.create(Dispatcher.class), "serv");

            while(true) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    System.out.println("Thread.sleep exception");
                }
            }
    }
}
